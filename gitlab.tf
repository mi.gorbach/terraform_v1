terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.3.0"
    }
  }
}

variable "gitlab" {}

provider "gitlab" {
    token = var.gitlab.gitlab_token
}

resource "gitlab_project" "terraform_project" {
    name = "new_terraform1"
    namespace_id = var.gitlab.namespace
} 

resource "gitlab_deploy_key" "gorbach_new" {
    project  = "gitlab_project.terraform_project.id"
    title    = "my deploy key"
    key      = "var.gitlab.key"
    can_push = "true"
}
